from unittest import TestCase
from mafia.member import Member


class MemberTests(TestCase):
    def test_get_subordinates(self):
        member = Member()
        subordinates = member.get_subordinates()
        self.assertCountEqual(subordinates, [])

    def test_set_subordinate(self):
        member = Member()
        member.set_subordinates(["Petrucci", "Cole"])
        subordinates = member.get_subordinates()
        self.assertEqual("Petrucci" in subordinates, True)
        self.assertEqual("Cole" in subordinates, True)

    def test_get_boss(self):
        member = Member()
        boss = member.get_boss()
        self.assertEqual(boss, "")

    def test_set_boss(self):
        member = Member()
        member.set_boss("Andy")
        boss = member.get_boss()
        self.assertEqual(boss, "Andy")

    def test_get_name(self):
        member = Member()
        name = member.get_name()
        self.assertEqual(name, "")

    def test_set_name(self):
        member = Member()
        member.set_name("Andy")
        name = member.get_name()
        self.assertEqual(name, "Andy")

    def test_get_seniority(self):
        member = Member()
        seniority = member.get_seniority()
        self.assertEqual(seniority, 0)

    def test_set_seniority(self):
        member = Member()
        member.set_seniority(5)
        seniority = member.get_seniority()
        self.assertEqual(seniority, 5)

