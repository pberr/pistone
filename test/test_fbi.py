from unittest import TestCase
from mafia.member import Member
from fbi.jail import Jail


class FbiTests(TestCase):
    def test_jail_in(self):
        jail = Jail()
        member = Member()
        member.set_name("Jhon")
        jail.enter(member)
        self.assertTrue(member in jail.prisioners)

    def test_jail_out(self):
        jail = Jail()
        member = Member()
        member.set_name("Jhon")
        jail.enter(member)
        capone = jail.exit("Jhon")
        self.assertEqual(member, capone)
