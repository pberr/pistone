from unittest import TestCase

from mafia.member import Member
from pistone import Pistone


class PistoneTests(TestCase):
    def test_populate_members(self):
        pistone = Pistone()
        pistone.populate_members()
        mafia = pistone.mafia_members
        for member in mafia:
            if member.get_name() == 'Jhon':
                self.assertEqual(member.get_name(), 'Jhon')
                self.assertEqual(member.get_boss(), 'Andy')
                self.assertEqual(member.get_seniority(), 4)
                self.assertEqual(member.get_subordinates(), ['Francis', 'Pascual'])

    def test_send_to_jail(self):
        pistone = Pistone()
        pistone.populate_members()
        mafia = pistone.mafia_members
        for member in mafia:
            if member.get_name() == 'Jhon':
                pistone.send_to_jail(member)
                self.assertTrue(member in pistone.jail.prisioners)

        member = Member()
        member.set_name('Jhon')
        member.set_boss('Andy')
        member.set_subordinates(['Francis', 'Pascual'])
        self.assertTrue(member not in pistone.mafia_members)

        for mafia_member in pistone.mafia_members:
            if mafia_member.get_name() == 'Carl':
                subordinates = mafia_member.get_subordinates()

                self.assertEqual(len(subordinates), 4)
                for mafioso in ['Francis', 'Pascual', 'Petrucci', 'Cole']:
                    self.assertTrue(mafioso in subordinates)


    def test_get_out_of_jail(self):
        pistone = Pistone()
        pistone.populate_members()
        mafia = pistone.mafia_members
        for member in mafia:
            if member.get_name() == 'Jhon':
                pistone.send_to_jail(member)
        jhon = pistone.get_out_of_jail('Jhon')

        pistone.get_root(jhon, 1)
        self.assertEqual(
            pistone.mafia_boss,
            'Andy'
        )

        self.assertTrue(jhon in pistone.mafia_members)

    def test_get_mafia_boss(self):
        pistone = Pistone()
        pistone.populate_members()

        member = Member()
        member.set_name('Carl')
        member.set_seniority(3)
        member.set_boss('Andy')
        member.set_subordinates(["Petrucci", "Cole"])
        pistone.get_root(member, 1)
        self.assertEqual(pistone.mafia_boss, 'Andy')

    # def test_print_org(self):
    #     agent = Pistone()
    #     agent.populate_members()
    #     agent.print_org()