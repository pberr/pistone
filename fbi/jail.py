
class Jail(object):
    def __init__(self):
        self.prisioners = set()

    def enter(self, member):
        self.prisioners.add(member)

    def exit(self, name):
        for member in self.prisioners:
            member_name = member.get_name()
            if member_name == name:
                self.prisioners.remove(member)
                return member
