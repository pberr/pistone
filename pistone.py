from mafia.member import Member
from fbi.jail import Jail
import json


class Pistone(object):
    def __init__(self):
        self.mafia_members = []
        self.jail = Jail()
        self.mafia_boss = ''
        self.mafia_bosses = []

    def populate_members(self):
        with open("bin/datos.json", "r") as read_file:
            data = json.load(read_file)
            # print(json.dumps((data), indent=2 ))
            for json_member in data['members']:
                member = Member()
                member.set_name(json_member['name'])
                member.set_seniority(json_member['seniority'])
                member.set_boss(json_member['boss'])
                member.set_subordinates(list(json_member['subordinates']))
                self.mafia_members.append(member)

    def send_to_jail(self, member):
        self.jail.enter(member)
        self.mafia_members.remove(member)
        m_name = member.get_name()
        if member.get_boss() != '':
            boss = self.name2member(member.get_boss())
            boss_subs = boss.get_subordinates()
            if m_name in boss_subs:
                boss_subs.remove(m_name)
                pass

            for p in self.mafia_members:
                if member.get_boss() == p.get_boss():
                    #print('PPPPPPPPP' + p.get_name())
                    if member.get_seniority() >= p.get_seniority() and member.get_name != p.get_name():
                        subordinates = member.get_subordinates()
                        new_subs = p.get_subordinates() + subordinates
                        for sub in subordinates:
                            subordinate = self.name2member(sub)
                            subordinate.set_boss(p.get_name())
                        p.set_subordinates(new_subs)
        else:
            for subordinate in member.get_subordinates():
                sub = self.name2member(subordinate)



    def get_out_of_jail(self, name):
        for prisioner in self.jail.prisioners:
            if prisioner.get_name() == name:
                boss = self.name2member(prisioner.get_boss())
                boss_subs = boss.get_subordinates()
                boss_subs.append(prisioner.get_name())
                boss.set_subordinates(boss_subs)
                prisioner_subordinates = prisioner.get_subordinates()
                self.mafia_members.append(prisioner)

                for subordinate in prisioner_subordinates:
                    for member in self.mafia_members:
                        if member.get_name() == subordinate and member.get_name() != prisioner.get_name():

                            old_boss = self.name2member(member.get_boss())
                            old_boss_subs = list(set(old_boss.get_subordinates()) - set(prisioner_subordinates))
                            old_boss.set_subordinates(old_boss_subs)
                            member.set_boss(prisioner.get_name())

                            self.jail.exit(member.get_name())
                return prisioner

    def get_leave(self):
        for m in self.mafia_members:
            if not m.get_subordinates():
                return m

    def get_root(self, member, i):
        boss = member.get_boss()
        if boss == '':
            self.mafia_boss = member.get_name()
        else:
            m = self.name2member(boss)
            self.get_root(m, i+1)



    def name2member(self, name):
        for member in self.mafia_members:
            if member.get_name() == name:
                return member

    def get_total_subordinates(self, member):
        if member.get_subordinates():
            count = len(member.get_subordinates())
            for sub in member.get_subordinates():
                subordinate = self.name2member(sub)
                count += self.get_total_subordinates(subordinate)
            return count
        else:
            return 0

    def get_bosses(self):
        self.mafia_bosses = list(filter(lambda member: self.get_total_subordinates(member) >= 4, self.mafia_members))

    def print_org(self, member):
        if member.get_subordinates():
            print(member.get_name())
            print(' ' + " ".join(member.get_subordinates()))
            for m in member.get_subordinates():
                # print(' ' + m)
                self.print_org(self.name2member(m))



def main():
    pistone = Pistone()
    pistone.populate_members()
    jhon = pistone.name2member("Jhon")
    pistone.get_root(jhon, 1)

    mafia_boss = pistone.name2member(pistone.mafia_boss)
    print('Org at the beggining')
    pistone.print_org(pistone.name2member(pistone.mafia_boss))
    print("\n")

    pistone.send_to_jail(jhon)
    print('Org after send member Jhon to prision')
    pistone.print_org(mafia_boss)
    print("\n")

    pistone.get_bosses()
    print(' Mafia Bosses with more than 4 members (without general boss and after send Jhon to prision): ')
    for boss in pistone.mafia_bosses:
        print(boss.get_name())
    print("\n")

    pistone.get_out_of_jail("Jhon")
    print('Org after get out member Jhon From prision')
    pistone.print_org(mafia_boss)
    print("\n")

    print("\n Mafia Boss: ", pistone.mafia_boss, "\n")
    pistone.get_bosses()
    print(' Mafia Bosses with more than 4 members (without general boss):')
    for boss in pistone.mafia_bosses:
        if boss != pistone.mafia_boss:
            print('  ' + boss.get_name())

if __name__ == "__main__":
    main()

