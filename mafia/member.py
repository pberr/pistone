class Member(object):
    def __init__(self):
        self.__name__ = ""
        self.__seniority__ = 0
        self.__boss__ = ""
        self.__subordinates__ = []

    def get_subordinates(self):
        return self.__subordinates__

    def set_subordinates(self, subordinates):
        self.__subordinates__ = subordinates

    def get_boss(self):
        return self.__boss__

    def set_boss(self, boss):
        self.__boss__ = boss

    def get_name(self):
        return self.__name__

    def set_name(self, name):
        self.__name__ = name

    def get_seniority(self):
        return self.__seniority__

    def set_seniority(self, seniority):
        self.__seniority__ = seniority
